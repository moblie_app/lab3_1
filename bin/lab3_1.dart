import 'package:lab3_1/lab3_1.dart' as lab3_1;

void main(List<String> arguments) {
  calculator cal = new calculator();
  double x = 8;
  double y = 2;
  double addResult = cal.add(x, y);
  double subResult = cal.substract(x, y);
  double multiplyResult = cal.multiply(x, y);
  double divideResult = cal.divide(x, y);
  double modResult = cal.modulo(x, y);
  double expoResult = cal.exponent(x, y);
  print('sum = $addResult');
  print('substract = $subResult');
  print('multiply = $multiplyResult');
  print('divide = $divideResult');
  print('modulo = $modResult');
  print('exponent = $expoResult');
}

class calculator {
  double result = 0;

  double add(double x, double y) {
    return result = x + y;
  }

  double substract(double x, double y) {
    return result = x - y;
  }

  double multiply(double x, double y) {
    return result = x * y;
  }

  double divide(double x, double y) {
    return result = x / y;
  }

  double modulo(double x, double y) {
    return result = x % y;
  }

  double exponent(double x, double y) {
    double base = x;
    double expo = y;
    double result = 1;
    for (int i=0; i < expo; i++){
      result *= base;
    }
    return result;
  }
}
